/**
 * Created by kompo on 11/09/2020
 */

const mongoose = require('mongoose');

const movieModel = mongoose.Schema({
    title: {
        type: String,
        required: '{PATH} is required!'
    },
    description: {
        type: String,
        required: '{PATH} is required!'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    likes: [
        {type:
            mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    dislikes: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    ],
    likeCount: {
        type: Number
    },
    dislikeCount: {
        type: Number
    }
},{
    timestamps: true
});

module.exports = mongoose.model('Movie',movieModel);
