/**
 * Created by kompo on 11/09/2020
 */

const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

const userModel = mongoose.Schema({
    name: {
        type: String,
        required: '{PATH} is required!'
    },
    movies: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'Movie'}
    ],
    likes: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'Movie'}
    ],
    dislikes: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'Movie'}
    ]
});

userModel.plugin(passportLocalMongoose);

module.exports = mongoose.model('User',userModel);
