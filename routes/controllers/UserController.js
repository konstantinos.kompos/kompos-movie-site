/**
 * Created by kompo on 11/09/2020
 */

const User = require('../../model/User');

const UserController = {
    async list() {
        const users = await User
            .find();
        return users;
    },
    async find(userName) {
        const user = await User
            .findOne({username: userName});
        return user;
    },
    async findUser(id) {
        const user = await User
            .findById(id);
        return user;
    }
};

module.exports = UserController;
