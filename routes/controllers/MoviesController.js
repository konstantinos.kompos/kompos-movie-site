/**
 * Created by kompo on 11/09/2020
 */

const Movie = require('../../model/Movie');

const MovieController = {
    async list() {
        const movies = await Movie
            .find()
            .populate('user');
        return movies;
    },
    async findMovie(id) {
        const movie = await Movie
            .findById(id)
            .populate('user');
        return movie;
    },
    async findByUser(user) {
        const movies = await Movie
            .find({user: user})
            .populate('user');
        return movies;
    },
    async sortByDate() {
        const movies = await Movie
            .find()
            .populate('user')
            .sort({createdAt: -1});
        return movies;
    },
    async sortByLikes() {
        const movies = await Movie
            .find()
            .populate('user')
            .sort({likeCount: -1});
        return movies;
    },
    async sortByDislikes() {
        const movies = await Movie
            .find()
            .populate('user')
            .sort({dislikeCount: -1});
        return movies;
    },
    async sortByUserAndDate(user) {
        const movies = await Movie
            .find({user: user})
            .populate('user')
            .sort({createdAt: -1});
        return movies;
    },
    async sortByUserAndLikes(user) {
        const movies = await Movie
            .find({user: user})
            .populate('user')
            .sort({likeCount: -1});
        return movies;
    },
    async sortByUserAndDislikes(user) {
        const movies = await Movie
            .find({user: user})
            .populate('user')
            .sort({dislikeCount: -1});
        return movies;
    }
};

module.exports = MovieController;
