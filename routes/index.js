const express = require('express');
const mqtt = require('mqtt');

const router = express.Router();
const MovieController = require('./controllers/MoviesController');
const UserController = require('./controllers/UserController');

const passport = require('passport');
const User = require('../model/User');
const Movie = require('../model/Movie');
const mqttOptions = require('../config/mqtt-server-configuration');


const mqttClient = mqtt.connect(mqttOptions.mqttClientAddress,mqttOptions.mqttOptions);

mqttClient.on('connect', function () {
    console.log('connected');
});

/* GET home page. */
router.get('/', async function (req, res, next) {
    let movies = await MovieController.sortByDate();

    res.render('index', {user: req.user, movies: movies, filter: null});
});

/* GET home page. */
router.get('/filter/:id', async function (req, res, next) {
    let user = await UserController.findUser(req.params.id);

    let movies = await MovieController.findByUser(user);

    res.render('index', {user: req.user, movies: movies, filter: req.params.id});
});

router.get('/order/:type?/:id?', async function (req, res, next) {
    let user;

    if (req.params.id) {
        user = await UserController.findUser(req.params.id);
    }

    let movies;
    if (!user) {
        if (req.params.type) {
            switch (req.params.type) {
                case 'likes':
                    movies = await MovieController.sortByLikes();
                    break;
                case 'hates':
                    movies = await MovieController.sortByDislikes();
                    break;
                default:
                    movies = await MovieController.sortByDate();
                    break;
            }
        } else {
            movies = await MovieController.sortByDate();
        }
    } else {
        if (req.params.type) {
            switch (req.params.type) {
                case 'likes':
                    movies = await MovieController.sortByUserAndLikes(user);
                    break;
                case 'hates':
                    movies = await MovieController.sortByUserAndDislikes(user);
                    break;
                default:
                    movies = await MovieController.sortByUserAndDate(user);
                    break;
            }
        } else {
            movies = await MovieController.sortByUserAndDate(user);
        }
    }

    res.render('index', {user: req.user, movies: movies, filter: req.params.id});
});

router.get('/register', function (req, res) {
    if (req.user)
        res.redirect('/');

    res.render('register');
});

router.post('/register', function (req, res, next) {
    console.log('registering user');
    User.register(new User({username: req.body.username, name: req.body.name}), req.body.password, function (err) {
        if (err) {
            console.log('error while user register!', err.message);
            res.render('register',{error:err.message});
            return;
        }

        console.log('user registered!');

        res.redirect('/login');
    });
});

router.get('/login', function (req, res) {
    if (req.user)
        res.redirect('/');

    res.render('login');
});

// router.post('/login', passport.authenticate('local', {
//     failureRedirect: '/login',
//     failureFlash: false
// }), function (req, res) {
//     res.redirect('/');
// });

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) {
            res.render('login',{error:err.message});
            return; // will generate a 500 error
        }
        // Generate a JSON response reflecting authentication status
        if (! user) {
            res.render('login',{error:"Wrong Credentials Provided"});
            return;
        }
        // ***********************************************************************
        // "Note that when using a custom callback, it becomes the application's
        // responsibility to establish a session (by calling req.login()) and send
        // a response."
        // Source: http://passportjs.org/docs
        // ***********************************************************************
        req.login(user, loginErr => {
            if (loginErr) {
                return next(loginErr);
            }
            res.redirect('/');
            return;
        });
    })(req, res, next);
});

router.get('/reset', function (req, res) {
    if (req.user)
        res.redirect('/');

    res.render('reset');
});

router.post('/reset', async function (req, res) {
    if (req.user)
        res.redirect('/');

    UserController.find(req.body.username).then(function(sanitizedUser){
        if (sanitizedUser){
            sanitizedUser.setPassword(req.body.password, function(){
                sanitizedUser.save();
                res.redirect('/login');
            });
        } else {
            res.render('reset',{error:'Wrong Username!'});
        }
    },function(err){
        console.error(err);
        res.render('reset',{error:err.message});
    });
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/likeMovie/:id', async function (req, res, next) {
    if (!req.user) {
        return res.send({ success : false, message : 'no user' });
    }

    let movie = await MovieController.findMovie(req.params.id);

    if (!movie) {
        return res.send({ success : false, message : 'no movie' });
    }

    // Check if user has already voted negatively for that movie and delete dislike
    if (req.user.dislikes.includes(movie._id)) {
        while (req.user.dislikes.includes(movie._id)) {
            req.user.dislikes.pop(movie._id);
        }

        while (movie.dislikes.includes(req.user._id)) {
            movie.dislikes.pop(req.user._id);
        }

        movie.dislikeCount = movie.dislikes.length;
        await req.user.save();
        await movie.save();
    }

    // Check if user has already liked that movie and remove vote
    if (req.user.likes.includes(movie._id)) {

        while (req.user.likes.includes(movie._id)) {
            req.user.likes.pop(movie._id);
        }

        while (movie.likes.includes(req.user._id)) {
            movie.likes.pop(req.user._id);
        }

        movie.likeCount = movie.likes.length;
        await req.user.save();
        await movie.save();
        mqttClient.publish('/movie/update',JSON.stringify({success: true, data: movie, hates: '', likes: ''}));
        return res.send({success: true, data: movie, hates: '', likes: ''});
    }

    // In any different case add user votes to likes
    await movie.likes.push(req.user._id);
    movie.likeCount = movie.likes.length;
    await req.user.likes.push(movie._id);
    await movie.save();
    await req.user.save();
    mqttClient.publish('/movie/update',JSON.stringify({success: true, data: movie, hates: '', likes: 'active'}));
    return res.send({success: true, data: movie, hates: '', likes: 'active'});
});

router.get('/hateMovie/:id', async function (req, res, next) {
    // Check if user is connected
    if (!req.user) {
        return res.send({ success : false, message : 'no user' });
    }

    let user = await UserController.findUser(req.user._id);

    // Check if movie exists
    let movie = await MovieController.findMovie(req.params.id);

    if (!movie) {
        return res.send({ success : false, message : 'no movie' });
    }

    //    Check if user has already liked that movie and remove that vote
    if (user.likes.includes(movie._id)) {
        while (user.likes.includes(movie._id)) {
            user.likes.pop(movie._id);
        }

        while (movie.likes.includes(user._id)) {
            movie.likes.pop(user._id);
        }

        movie.likeCount = movie.likes.length;

        await user.save();
        await movie.save();
    }

    // Check if user has already disliked that movie and remove that vote
    if (user.dislikes.includes(movie._id)) {
        while (user.dislikes.includes(movie._id)) {
            user.dislikes.pop(movie._id);
        }

        await user.save();

        while (movie.dislikes.includes(user._id)) {
            movie.dislikes.pop(user._id);
        }

        movie.dislikeCount = movie.dislikes.length;

        await movie.save();

        mqttClient.publish('/movie/update',JSON.stringify({success: true, data: movie, hates: '', likes: ''}));

        return res.send({success: true, data: movie, hates: '', likes: ''});
    }

    await user.dislikes.push(movie._id);
    await user.save();

    // In any different case submit user vote
    await movie.dislikes.push(user._id);
    movie.dislikeCount = movie.dislikes.length;
    await movie.save();

    mqttClient.publish('/movie/update',JSON.stringify({success: true, data: movie, hates: 'active', likes: ''}));
    return res.send({success: true, data: movie, hates: 'active', likes: ''});
});

router.post('/addMovie', async function (req, res, next) {
    console.log('Adding Movie');
    if (!req.user)
        res.redirect('/');

    let user = await UserController.findUser(req.user._id);

    let movie = new Movie({title: req.body.title, description: req.body.description, user: user._id});

    movie.likeCount = 0;
    movie.dislikeCount = 0;

    await movie.save();
    await user.movies.push(movie._id);
    await user.save();

    mqttClient.publish('/movie/update',JSON.stringify({success: true, data: movie, user: user, likes: ''}));
    res.redirect('/');
});

router.get('/getMovies', async function (req,res,next) {
    let movies = await MovieController.sortByDate();
    return res.send({data: movies});
});

module.exports = router;
