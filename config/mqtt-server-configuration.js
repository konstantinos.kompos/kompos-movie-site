/**
 * Created by kompo on 16/09/2020
 */

const mqttOptions = {
    keepalive:10,
    clientId:'mqttjs_' + Math.random().toString(16).substr(2, 8),
    clean:true,
    reconnectPeriod:1000,
};

const mqttClientAddress = 'mqtt://localhost';

module.exports.mqttOptions = mqttOptions;
module.exports.mqttClientAddress = mqttClientAddress;
