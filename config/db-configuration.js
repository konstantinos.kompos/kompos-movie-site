/**
 * Created by kompo on 16/09/2020
 */

const db_url = 'mongodb://localhost/my_database';

const db_options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
};

module.exports.db_url = db_url;
module.exports.db_options = db_options;
