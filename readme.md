# Movierama Project

## Description
The application is (yet another) social sharing platform where users can share their favorite movies. Each movie has a title and a small description as well as a date that corresponds to the date it was added to the database.
In addition it holds a reference to the user that submitted it. Users can also express their opinion about a movie by either likes or hates.
When an unregistered user visits the homepage, they see the list of movies that have been added to the system, as well as links for logging in or signing up. Each movie contains the following information:
- Title
- Description
- Name of the user
- Date of publication
- Number of likes
- Number of hates

The name of the user is a link that, when clicked upon, filters the movies down to the items that have been shared by this specific user. The list can also be sorted by the number of likes, number of hates or the publication date.
In addition to the functionality described above, registered users are able to add their own movies and express their opinion for other movies by either a like or a hate. Voting is performed by clicking on the respective counters that are displayed for each movie.
## Features
In general it is a NodeJS Express application offering the following feature:
- User Management
- MongoDB local storage, with all the necessary relationships implemented
- Asynchronous update message broadcasting to each active application instances

### User Management
User management is based on [Passport-Local Mongoose](https://www.npmjs.com/package/passport-local-mongoose) package.
It offers user registration, user login and user password reset. Registered users are safelly stored to the connected local DB.

### MongoDB Local Storage
A MVC apprach was implemented capable of handling different relationships as shown in the following diagram 
#### DB Model View
![DB Model View](db_model.png)

### Message BroadCasting
In order to achieve application updates in real time when a user action, either vote or movie insert performed, a public MQTT message broker was included.
Specifically, when one of those events triggered by a user, the application server publishes a message to a predefined topic via that message broker.
All active application instances have an active are subscribed to that topic and thus they receive published messages.
Once a message is received, a javascript function run on the client side, which updates the user interface accordingly.

## Test the application
You can test application through the following link:
[http://www.kompos-iot.xyz](http://www.kompos-iot.xyz) 
## Installation
### Requirements
Application has the following installation requirements:
- MongoDB Connection
    - Connection properties can be configured in the [/config/db-configuration.js](/config/db-configuration.js)
- Public MQTT message broker
    - Connection properties for the application server can be configured in the [/config/mqtt-server-configuration.js](/config/mqtt-server-configuration.js)
    - Connection properties for the clients can be configured in the [/config/mqtt-client-configuration.js](/config/mqtt-client-configuration.js)
- NodeJS installed on the local machine where the application will run
### Installation
1. Clone the project
2. Pass configuration parameters for both MongoDB and MQTT
3. run 'npm install' on project's root folder
4. run npm start on project's root folder
