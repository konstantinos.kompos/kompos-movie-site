/**
 * Created by kompo on 12/09/2020
 */

import * as mqtt_client_options from '/config/mqtt-client-configuration.js';

// const host = '135.181.88.18';
// const port = 1884;
let mqttClient;

$(document).ready(function () {

    document.addEventListener('visibilitychange', handleVisibilityChange, false);

    $('time.timeago').timeago();

    MQTTconnect();

    $('.vote').on('click', function () {
        if (this.classList.contains('disabled')){
            return;
        }

        let movieID = $(this).parent().attr('id');

        let url = this.classList.contains('hate') ? '/hateMovie/'+movieID : '/likeMovie/'+movieID;

        $.get(url,function (data,status) {
            if (!data.success)
                return;
            updateElements(data);
            return;
        });
    });

    function MQTTconnect() {
        mqttClient = new Paho.MQTT.Client(mqtt_client_options.host, mqtt_client_options.port, new Date().getTime().toString());
        var options = {
            timeout: 3,
            onSuccess: onConnect,
            onFailure: onFailure
        };

        mqttClient.onMessageArrived = onMessage;

        mqttClient.connect(options);
    }

    function onConnect() {
        console.log("Connected")
        mqttClient.subscribe('/movie/update');
    }

    function onFailure(message) {
        console.log("Connection Attempt to Host " + host + ": Failed");
    }

    function onMessage(msg) {
        let movieObject = JSON.parse(msg.payloadString);

        console.log(movieObject);

        let movieContainer = $('#movieContent').find('#'+movieObject.data._id);

        if (movieContainer.length > 0)
            return updateElements(movieObject);

        return createElement(movieObject);
    }

    function mqttDisconnect() {
        mqttClient.disconnect();
    }

    // async function updateView() {
    //     console.log("Get Movies");
    //     $.get('/getMovies',function (data,status) {
    //         console.log(data);
    //         data.data.forEach(function (movie) {
    //             let movieContainer = $('#movieContent').find('#'+movie._id);
    //
    //         });
    //         return;
    //     });
    // }

    function handleVisibilityChange() {
        if (document.visibilityState == "hidden") {
            mqttDisconnect();
        } else {
            // updateView();
            MQTTconnect();
        }
    }

    function updateElements(movieResponse) {
        let movieElement = movieResponse.data;
        console.log(movieElement);
        console.log(movieElement.likes);

        let movieVotes = document.getElementById(movieElement._id);
        console.log(movieVotes);
        let userID = $('#user').attr('userid');
        $(movieVotes).find('.likes').html(movieElement.likeCount);
        $(movieVotes).find('.dislikes').html(movieElement.dislikeCount);
        $(movieVotes).find('.vote').removeClass('active');

        if (!userID || movieElement.user._id === userID) {
            $(movieVotes).find('.vote').addClass('disabled');
        } else {
            console.log(movieElement.likes);
            $(movieVotes).find('.like').addClass(movieResponse.likes);
            $(movieVotes).find('.hate').addClass(movieResponse.hates);
        }

        return;
    }

    async function createElement(movieElement) {
        console.log('New Element');
        let userID = $('#user').attr('userid');
        let movieInfo = movieElement.data;
        console.log(movieInfo);

        let card = document.createElement('div');
        $(card).addClass('card');

        let cardBody = document.createElement('div');
        $(cardBody).addClass('card-body');

        let cardTitle = document.createElement('h5');
        $(cardTitle).addClass('card-title');
        $(cardTitle).addClass('text-center');
        $(cardTitle).html(movieInfo.title);

        cardBody.appendChild(cardTitle);

        let cardSubtitle = document.createElement("h6");
        $(cardSubtitle).addClass('card-subtitle');
        $(cardSubtitle).addClass('mb-2');
        $(cardSubtitle).addClass('text-muted');
        $(cardSubtitle).addClass('text-center');

        let span1 = document.createElement('span');
        $(span1).html('Created by');

        let span2 = document.createElement('span');
        $(span2).addClass('pl-1');
        let userLink = document.createElement('a');
        userLink.setAttribute('href','/filter/' + movieElement.user._id)
        $(userLink).html(movieElement.user.name);
        span2.appendChild(userLink);

        let timeCont = document.createElement('time');
        $(timeCont).addClass('pl-1');
        $(timeCont).addClass('timeago');
        timeCont.setAttribute('datetime',movieInfo.createdAt);

        cardSubtitle.appendChild(span1);
        cardSubtitle.appendChild(span2);
        cardSubtitle.appendChild(timeCont);

        cardBody.appendChild(cardSubtitle);

        let cardDescritpion = document.createElement('p');
        $(cardDescritpion).addClass('card-text');
        $(cardDescritpion).html(movieInfo.description);

        cardBody.appendChild(cardDescritpion);

        let voteRow = document.createElement('div');
        $(voteRow).addClass('row');
        $(voteRow).addClass('no-gutters');
        voteRow.setAttribute('id',movieInfo._id);

        let likeCounter = document.createElement('div');
        $(likeCounter).addClass('col-4');
        $(likeCounter).addClass('col-sm-3');
        $(likeCounter).addClass('col-md-2');
        $(likeCounter).addClass('vote');
        $(likeCounter).addClass('like');
        $(likeCounter).addClass('p-2');
        $(likeCounter).addClass('mr-1');
        $(likeCounter).addClass('like');
        $(likeCounter).addClass('text-center');

        if (!userID || movieElement.user._id === userID)
            $(likeCounter).addClass('disabled');

        let thumbsUp = document.createElement('i');
        $(thumbsUp).addClass('far');
        $(thumbsUp).addClass('fa-thumbs-up');
        thumbsUp.setAttribute('aria-hidden','true');

        let likeSpan = document.createElement('span');
        $(likeSpan).addClass('likes');
        $(likeSpan).html(movieInfo.likeCount);

        likeCounter.appendChild(thumbsUp);
        likeCounter.appendChild(likeSpan);

        let dislikeCounter = document.createElement('div');
        $(dislikeCounter).addClass('col-4');
        $(dislikeCounter).addClass('col-sm-3');
        $(dislikeCounter).addClass('col-md-2');
        $(dislikeCounter).addClass('vote');
        $(dislikeCounter).addClass('like');
        $(dislikeCounter).addClass('p-2');
        $(dislikeCounter).addClass('mr-1');
        $(dislikeCounter).addClass('hate');
        $(dislikeCounter).addClass('text-center');

        if (!userID || movieElement.user._id === userID)
            $(dislikeCounter).addClass('disabled');

        let thumbsDown = document.createElement('i');
        $(thumbsDown).addClass('far');
        $(thumbsDown).addClass('fa-thumbs-down');
        thumbsDown.setAttribute('aria-hidden','true');

        let hateSpan = document.createElement('span');
        $(hateSpan).addClass('dislikes');
        $(hateSpan).html(movieInfo.dislikeCount);

        dislikeCounter.appendChild(thumbsDown);
        dislikeCounter.appendChild(hateSpan);

        voteRow.appendChild(likeCounter);
        voteRow.appendChild(dislikeCounter);

        cardBody.appendChild(voteRow);

        card.appendChild(cardBody);

        console.log(card);


        document.getElementById('cardColumns').appendChild(card);

        $('time.timeago').timeago();
    }
});
